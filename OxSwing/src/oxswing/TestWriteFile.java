/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxswing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Taneat
 */
public class TestWriteFile {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        File file = null;
        ObjectOutputStream oos = null;
            Player o = new Player('o');
            Player x = new Player('x');
            o.countWin();
            x.countLose();
            o.countDraw();
            x.countDraw();
        try {
            file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
            
        } finally {
            try {
                if(fos !=null) fos.close();
                if(oos !=null) oos.close();
            } catch (IOException ex) {
                
            }
        }
    }
}
