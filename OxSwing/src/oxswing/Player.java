/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxswing;

import java.io.Serializable;

/**
 *
 * @author Taneat
 */

public class Player implements Serializable{
	private char name;
	private int win = 0; 
	private int lose = 0;
	private int draw = 0;
	
	
	public Player() {
		
	}
	public Player(char name) {
		this.name=name;
	}
	public char getName() {
		return name;
	}
	public void setName(char name) {
		this.name=name;
	}
	public int getWin() {
		return win;
	}
	public int getLose() {
		return lose;
	}
	public int getDraw() {
		return draw;
	} 
	public void countWin() {
		this.win++;
	}
	public void countLose() {
		this.lose++;
	}
	public void countDraw() {
		this.draw++;
	}

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
        
}


