/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxswing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Taneat
 */
public class PlayerService {
    static Player o,x;
    static{
        o = new Player('o');
        x = new Player('x');
    }
    public static void load(){
        FileInputStream fis = null;
        File file = null;
        ObjectInputStream ois = null;
        try {
            file = new File("ox.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            o = (Player)ois.readObject();
            x = (Player)ois.readObject();
            
        } catch (FileNotFoundException ex) {
           
        } catch (IOException ex) {
            
        } catch (ClassNotFoundException ex) {
            
        }
        System.out.println(o);
        System.out.println(x);
    }
    public static void save(){
        FileOutputStream fos = null;
        File file = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
            
        } finally {
            try {
                if(fos !=null) fos.close();
                if(oos !=null) oos.close();
            } catch (IOException ex) {
                
            }
        }
    }

    public static Player getO() {
        return o;
    }

    public static Player getX() {
        return x;
    }
    
}
